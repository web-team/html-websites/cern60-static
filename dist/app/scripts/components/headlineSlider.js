
define(function(require){

    'use strict';

    var $ = require('jquery');

      
    var itemSelector =  '.m-headline__item';
    var allItems =      $(itemSelector);
    var activeClass =   'm-headline__item--is-active';
    var delay =         500; // first slide
    var normalDelay =   4000;
    
    var tick = function(next) {

      setTimeout(function(){

        allItems
          .removeClass(activeClass)
          .eq(next)
            .addClass(activeClass);

        if (next == 3) next = -1;
        
        tick(next+1);
      
      }, delay);      
      
      delay = normalDelay;
    };
    
    tick(0);

});