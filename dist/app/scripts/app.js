/*!
 * PROJECTNAME
 * Main application file compiled with r.js via Grunt
 */
require.config({
    urlArgs: "bust=" + (new Date()).getTime(),
    paths: {
        'jquery': '../../../vendor/jquery.min',
        'jqueryScrollTo': '../../../vendor/jquery.scrollTo.min',
        'jqueryValidate': '../../../vendor/jquery.validate.min',
        'jqueryParallax': '../../../vendor/jquery.jparallax',

        /* Components
         ****************************/
        'consoleShim': 'components/consoleShim',
        'externalLinks': 'components/externalLinks',
        'headlineSlider': 'components/headlineSlider'
    },
    shim: {
        jquery: {
            exports: 'jQuery'
        },
        jqueryParallax: {
            exports: 'jQuery',
            deps: ['jquery']
        },
        jqueryScrollTo: {
            exports: 'jQuery',
            deps: ['jquery']
        }
    },
    deps: ['require', 
        'jquery', 
        //'jqueryParallax', 
        'jqueryScrollTo'],
    callback: function(require, $) {
        'use strict';

        // Load all those common modules at once
        require(['consoleShim']);
        //require(['jqueryScrollTo']);
        require(['jqueryValidate']);
        require(['externalLinks']);
        require(['headlineSlider']);


        $('.js-scrollto-events-trigger').on('click',function(){
            $('body').stop().scrollTo('.js-scrollto-events-target', 500, {offset: {top:-20}});
        });

        $(function(){

            if ( location.hash.split('#')[1] == 'newsletter' ){
                $('body').stop().scrollTo('.js-scrollto-newsletter-target', 500, {offset: {top:-20}});
                $('.m-newsletter').addClass('m-newsletter--success');
            }

        // console.log('parallax');
        // setTimeout(function(){
        //     $('.m-nodes__layer').parallax({
        //         mouseport: $('body'),
        //         xparallax: .1,
        //         yparallax: .5,
        //         xorigin: 0.5, 
        //         yorigin: 0.5
        //     });
        //     console.log('parallax end');
        // }, 2000);

        // Resolving unfixed bug in jquery.parallax
        $('body').on('mousemove', function(){
            $('.m-nodes__viewport').animate({
                opacity:1
            }, 1000);
        });

        $('body').on('touchstart', function(){
            $('.m-nodes__viewport').animate({
                opacity:1
            }, 1000);
        });

        }); // DOM:ready



        /* Load submodule
        ******************************************************/
        var filename = $('[data-main]').eq(0).data('module'),
            modulename;

        if (typeof filename !== 'undefined' && filename !== '') {
            modulename = [ 'pages', filename, 'main'].join('/');
            require([modulename]);
        } else {
            if (window.console) {
                console.info('no modulename found via data-module of requirejs.. idle');
            }
        }
    }
});